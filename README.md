# Notify.io

The service is a hackathon project created to help citizens:  
http://www.citizensofwallonia.be/

## How did we tried to help citizens life ?
We give the citizens a tool that allow him to report a subject just in case it could be usefull.
This report is store with a readonly access to the citizens and all the contacts he allowed.

The core concept is a API to archive a private and crypted report for reasons.
Those report can then be share later if need.

Lot's of application can be created on this service idea.

## Example

### Medicine
The user can record report on his account of his current illness.
When the user see its doctor, he can share the report to him only.
Giving the doctor photos, vocal records and videos to understand what is happening to you.

### Communal help

The user can notify to local authoritize the degradation of the public equipment.
He can use it to improve citizens live by allow access to this report to public establishement.
He can also use it to prove that an accident is not the fault of the user
but of the degradation that was reported since a long time.

### Police

The citizens hear a fireshot at 23:40 and see a car run away.
He write down the car number and link the photos to his private report.

The next day, the police make a public call for witness.
The user understand that he could help and share the information to the police.
He share by email to the police the link to the report with a key access.
The police received a prove with the photos, a description, a GPS position, the exact time of the crime,etc




## Credits:
Nella Denis 
Tayon Lola 
Hoffman Yan 
Roisin Thomas 
Wouters Amélie
Strée Eloi
