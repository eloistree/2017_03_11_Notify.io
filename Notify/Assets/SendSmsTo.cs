﻿using DeadMosquito.AndroidGoodies;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SendSmsTo : MonoBehaviour {

    public string _message;
    public string[] _numbersToCall;
    public string[] _mailAddresses;
    public bool _sendWithoutAsk=true;


    public void SendMailAndSMS() {

        string messageToSend = string.Format("Auto-Message: I could need medical attention! Please contact me immediatly. Call for help if I do not answer !!! (GPS POSITION: https://www.google.be/maps/@{0},{1},14z?hl=en) Time:{2} ",
               Input.location.lastData.longitude,
               Input.location.lastData.latitude,
               DateTime.Now);

       string  tweetMessage = string.Format("Could be in need of medical attention at https://www.google.be/maps/@{0},{1},14z?hl=en !",
               Input.location.lastData.longitude,
               Input.location.lastData.latitude);
        for (int i = 0; i < _numbersToCall.Length; i++)
        {
           
            AGShare.SendSms(_numbersToCall[i], messageToSend,!_sendWithoutAsk, "Need mecial attention, maybe unconsciou !");
           
        }
        
        AGShare.SendEmail(_mailAddresses, "Need mecial attention, maybe unconsciou !", messageToSend,null, !_sendWithoutAsk, "Need mecial attention, maybe unconsciou !");
        AGShare.Tweet(tweetMessage);
    }

}
