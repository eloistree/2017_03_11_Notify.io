﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelsManager : MonoBehaviour {

    [SerializeField]
    public NamedPanel[] _namedPanels;

	public void SetPanelTo (string idName) {
        for (int i = 0; i < _namedPanels.Length; i++)
        {
            if (_namedPanels[i].panel == null)
                continue;
            bool isActive = idName != null && _namedPanels[i].idName == idName;
            if (_namedPanels[i].animator != null)
            {
                _namedPanels[i].animator.SetBool("IsActive", isActive);
            }
            else {
                _namedPanels[i].panel.gameObject.SetActive(isActive);
            }
        }

    }
	
	public void HidePanels () {
        SetPanelTo(null);
	}
}

[System.Serializable]
public struct NamedPanel {
    public string idName;
    public RectTransform panel;
    public Animator animator;
}
