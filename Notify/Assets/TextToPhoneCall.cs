﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DeadMosquito.AndroidGoodies;
public class TextToPhoneCall : MonoBehaviour {

    public Text _numberToCall;

	public void Call () {
        if (_numberToCall != null) {
            AGDialer.PlacePhoneCall(_numberToCall.text);
        }
        
	}
	
}
