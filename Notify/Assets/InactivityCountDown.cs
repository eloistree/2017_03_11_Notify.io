﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class InactivityCountDown : MonoBehaviour {


    public float _timeToBeInactif=20;
    public float _timeLeft;
    public Image[] _affectedImage;

    public UnityEvent _toDoIFInactif;


    void Start() {
        _timeLeft = _timeToBeInactif;
    }


void Update () {

        if (Input.GetMouseButton(0) || Input.touchCount > 0)
            _timeLeft = _timeToBeInactif ;
        if (_timeLeft > 0f)
            _timeLeft -= Time.deltaTime;
        else if (_timeLeft < 0f) {
            _toDoIFInactif.Invoke();
            _timeLeft = 0;
        }
        for (int i = 0; i < _affectedImage.Length; i++)
        {
            _affectedImage[i].fillAmount = Mathf.Clamp(_timeLeft/_timeToBeInactif, 0.0f, 1.0f); ;
        }

    }
    public void ResetCountDown()
    {
        _timeLeft = _timeToBeInactif;

    }
}

