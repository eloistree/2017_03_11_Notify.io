﻿//
// Class Documentation: https://github.com/TarasOsiris/android-goodies-docs-PRO/wiki/AGContacts.cs
//

#if UNITY_ANDROID
using DeadMosquito.AndroidGoodies.Internal;
using System;

namespace DeadMosquito.AndroidGoodies
{
    /// <summary>
    /// Class to pick contacts.
    /// </summary>
    public static class AGContacts
    {
        /// <summary>
        /// Picks the contact from the phone contacts book.
        /// </summary>
        /// <param name="onSuccess">On success callback. Picked contact is passed as parameter</param>
        /// <param name="onFailure">On failure callback. Failure reason is passed as parameter</param>
        public static void PickContact(Action<ContactPickResult> onSuccess, Action<string> onFailure)
        {
            if (AGUtils.IsNotAndroidCheck())
            {
                return;
            }

            Check.Argument.IsNotNull(onSuccess, "onSuccess");
            Check.Argument.IsNotNull(onFailure, "onFailure");

            AGUtils.RunOnUiThread(
                () =>
                AGActivityUtils.PickContact(
                    new AGActivityUtils.OnPickContactListener(onSuccess, onFailure)));
        }
    }
}
#endif