﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class WaitAndSkipUpload : MonoBehaviour
{

    public Image _cloudLoadingImage;

    public float _initialTime = 4f;
    public float _timeLeft = 4f;

    public UnityEvent _toDoOnLoaded;

    public void Start()
    {
        _timeLeft = _initialTime;
    }
    public void OnEnable() {
        _timeLeft = _initialTime;
    }

    void Update()
    {

        if (_timeLeft < 0) return;
        _timeLeft -= Time.deltaTime;
        _cloudLoadingImage.fillAmount = Mathf.Clamp(_timeLeft / _initialTime, 0.0f, 1f);

        if (_timeLeft < 0f)
        {
            _toDoOnLoaded.Invoke();
        }
    }
}