﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EscapeListenerToFSM : MonoBehaviour {

    public PlayMakerFSM _panelsManager;
	void Update () {

        if (Input.GetKeyDown(KeyCode.Escape))
            _panelsManager.SendEvent("Back");


    }
}
