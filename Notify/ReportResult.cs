﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class ReportResult
{

    public string _title;
    public string _description;
    public double _latitudePosition;
    public double _longitudePosition;

    public string[] _audioFilePaths;
    public string[] _imageFilePaths;
    public string[] _videoFilePaths;
    public string[] _otherFilePaths;




}


public class DeviceInformation {

    public string _name;
    //Computer Stand Alone, Phone, , Facebook Watson ...
    public string _type;


}